const express = require('express');
const router = express.Router();
const controller = require("../controllers/users");

router.get('/', controller.getUsers);
router.get('/:id', controller.getUser);
router.post('/', controller.create);
router.put('/:id', controller.replace);
router.patch('/:id', controller.edit);
router.delete('/:id', controller.destroy);

module.exports = router;